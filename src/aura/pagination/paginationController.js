({
    initmethod : function (component, event, helper) {
        
        component.find("levels").set("v.value","All Opportunities");
        var action = component.get("c.getListViews");
        action.setCallback(this, function(response){
        var state = response.getState();
        console.log('clicked');
            if (state === "SUCCESS") {
                var listviews = response.getReturnValue();
                component.set("v.opplistview",listviews);
               	helper.onSelect(component);
             }
        });
        $A.enqueueAction(action);
       // String domainUrl = System.URL.getSalesforceBaseUrl();
         component.set("v.urldomainName",window.location.hostname);
        //console.log('domainUrl'+domainUrl);
    },
    onSelectChange : function(component, event, helper) {
        debugger;
        helper.onSelect(component);
               
	},
    onClickfirstPage: function(component, event, helper) {
            component.set("v.currentPageNumber", 1);
            component.set("v.pageNumber",1);
        if(component.get("v.pageNumber") === 1){
            component.set("v.firstclick",true);
            component.set("v.prevclick",true);
            component.set("v.nextclick",false);
            component.set("v.lastclick",false); 
            helper.onClickRenderPage(component);
        }
    },
    onClickprevPage: function(component, event, helper) {
        var pageN = component.get("v.pageNumber");
        pageN--;
        component.set("v.currentPageNumber", Math.max(component.get("v.currentPageNumber")-1, 1));
        component.set("v.pageNumber",pageN);
         if(component.get("v.pageNumber") === 1){
            component.set("v.firstclick",true);
            component.set("v.prevclick",true);
            component.set("v.nextclick",false);
            component.set("v.lastclick",false); 
            helper.onClickRenderPage(component);
        }else if(component.get("v.pageNumber") < component.get("v.maxPageNumber")){
            component.set("v.firstclick",false);
            component.set("v.prevclick",false);
            component.set("v.nextclick",false);
            component.set("v.lastclick",false);
            helper.onClickRenderPage(component);
        }
    },
    onClicknextPage: function(component, event, helper) {
        debugger;
        var pageN = component.get("v.pageNumber");
        pageN++;
        component.set("v.currentPageNumber", Math.min(component.get("v.currentPageNumber")+1, component.get("v.maxPageNumber")));
        component.set("v.pageNumber",pageN);
        if(component.get("v.pageNumber") < component.get("v.maxPageNumber")){
            component.set("v.firstclick",false);
            component.set("v.prevclick",false);
            component.set("v.nextclick",false);
            component.set("v.lastclick",false); 
            helper.onClickRenderPage(component);
        }else if(component.get("v.pageNumber") == component.get("v.maxPageNumber")){
            component.set("v.firstclick",false);
            component.set("v.prevclick",false);
            component.set("v.nextclick",true);
            component.set("v.lastclick",true); 
            helper.onClickRenderPage(component);
        }
              
    },
    onClicklastPage: function(component, event, helper) {
        component.set("v.currentPageNumber", component.get("v.maxPageNumber"));
        component.set("v.pageNumber",component.get("v.maxPageNumber"));
        if(component.get("v.pageNumber") == component.get("v.maxPageNumber")){
            component.set("v.firstclick",false);
            component.set("v.prevclick",false);
            component.set("v.nextclick",true);
            component.set("v.lastclick",true); 
            helper.onClickRenderPage(component);
        } 
    },
    applyCSS: function(cmp, event) {
        debugger;
        var selectedItem = event.currentTarget;
        var inputsel = selectedItem.dataset.record;
        var toggleText = cmp.find('text');
         $A.util.toggleClass(toggleText[inputsel], "toggle");
        
    }
})