({
	  
    doInit :function(component, event) {
        debugger;
        console.log("Tab>>> " + component.get("v.defaultTab"));
        
        var action = component.get("c.getcampignList");
        var recordId = component.get("v.recordId");
        action.setParams({recordId: recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result=response.getReturnValue();
               
                console.log("Success");
                component.set("v.hidetab",result);
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
        
    },
    handleChanged : function(component,evt,helper){
        debugger;
    var checkedList = component.find("checked");
    var result = checkedList.get("v.value");
   	component.set("v.checkboxsel",result);
   } ,
    onblur:  function(cmp, helper) {
    	console.log('venue layout blurred...');
	},
	onfocus:  function(cmp, helper) {
    	console.log('venue layout focussed...');
	},
    activateEventPage : function(){
        var appEvent = $A.get("e.c:componentCommunicationEvent");
        appEvent.setParams({
            "target" : "event"});
        appEvent.fire();
    },
    activateVenueList : function(){
        var appEvent = $A.get("e.c:componentCommunicationEvent");
        appEvent.setParams({
            "target" : "venuelist"});
        appEvent.fire();
    },
    activateVenueLayout : function(){
        var appEvent = $A.get("e.c:componentCommunicationEvent");
        appEvent.setParams({
            "target" : "venuelayout"});
        appEvent.fire();
    }
})