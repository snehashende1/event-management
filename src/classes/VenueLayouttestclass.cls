@isTest
public class VenueLayouttestclass {
    public static String LAYOUT_TYPE_ROUND = 'Round';
    public static String LAYOUT_TYPE_SQUARE = 'Square';
    public static String LAYOUT_TYPE_AUDIENCE = 'Audience';
     public static String LAYOUT_TYPE_CLASSROOM = 'Classroom';
     public static String LAYOUT_TYPE_BOOTH = 'Booth';
   
    @IsTest static void testvenuelayout(){
        
        Venue__c ven=new Venue__c();
        ven.Name='vashi';
        insert ven;
        System.assertEquals(true, ven.Id != null, ' No user created.' + ven) ;
       
        
        //List<venue__c> venid = [select id from venue__c];
        
        Venue_Layout__c venl=new Venue_Layout__c();
        venl.Name = 'abc';
        venl.Layout__c='custom';
        venl.Type__c='Round';
        venl.Venue__c=ven.Id;
         venl.No_of_Tables__c=10;
        venl.No_of_Seats__c=10;
        venl.Layout_Row_Count__c=10;
         venl.Seats_Per_Row__c=10;
        insert venl;
        System.assertEquals(true, venl.Id != null, ' No user created.' + venl) ;
        
        Campaign camp = new Campaign();
        camp.name = 'old Event';
        camp.All_Day_Event__c = false;
        camp.Start_Date__c = system.now();
        camp.End_Date__c = system.now();
        camp.Type = 'A,B,C';
        insert camp;
      	 System.assertEquals(true, camp.Id != null, ' No user created.' + camp) ;
        
        list<id> idList = new list<id>();
        for( Venue__c  venueObj : [Select id from Venue__c]){
            
            idList.add(venueObj.id);
        }
        
        
         test.startTest();
        Venuelayoutcontroller.getEventVenues(idList,camp.Id);
        Venuelayoutcontroller.getLayoutTypes();
         Venuelayoutcontroller.getCustomLayoutTypes();
        Venuelayoutcontroller.getPlainLayoutTypes();
        Venuelayoutcontroller.getConferenceTypes();
        Venuelayoutcontroller.saveEventVenueLayout(venl.Id, camp.Id,ven.Id);
        Venuelayoutcontroller.matchvenuelayout(venl, camp.Id, ven.Id, venl.Type__c);
        Venuelayoutcontroller.createEventVenueLayout(venl,camp.Id,ven.Id);
        Venuelayoutcontroller.generateGUID();
        
         test.stoptest();
        
        
        
        List<Venue_Layout__c> newlist = [select Id,Venue__c,Layout__c,Type__c from Venue_Layout__c where Venue__c =:ven.Id];
        system.assertEquals(ven.Id, newlist[0].Venue__c);
    }
    
    
    @isTest static void testvenuelayoutbulk(){
        
        List<Venue_Layout__c> venList = new List<Venue_Layout__c>();
        Venue_Layout__c venObj;
        for(Integer i = 0;i< 10;i++){
            venObj = new Venue_Layout__c();
            venObj.Name = 'Pune ' + i;
            
            venList.Add(venObj);
        }
        try{
            insert venList;
            System.assertEquals(true, venList[0].Id != null, ' Campaign is not created.' + venList);
            //Venuelayoutcontroller.getvenuelayoutList(venList[0].Id);
            List<Venue_Layout__c> newuser=[select Id,Name from Venue_Layout__c ];
            
            System.assertEquals(venList.size(),newuser.size());
        }
        catch(Exception e){
            System.debug('In exception - ' + e.getMessage());
            
        }  	
    }
    
    @IsTest static void matchInsertScenarioRound(){
        Campaign camp = new Campaign();
        camp.name = 'old Event';
        camp.All_Day_Event__c = false;
        camp.Start_Date__c = system.now();
        camp.End_Date__c = system.now();
        camp.Type = 'A,B,C';
        insert camp;
        
        Venue__c ven=new Venue__c();
        ven.Name='vashi';
        insert ven;
        
        Venue__c venue=new Venue__c();
        venue.Name='vashi';
        insert venue;
        
        Venue_Layout__c venl=new Venue_Layout__c();
        venl.Name = 'abc';
        venl.Layout__c='custom';
        venl.Type__c='Round';
        venl.Venue__c=venue.Id;
        venl.No_of_Tables__c=10;
        venl.No_of_Seats__c=10;
        venl.Layout_Row_Count__c=10;
        insert venl;
        
          test.startTest();
        Venuelayoutcontroller.matchvenuelayout(venl, camp.Id, ven.Id, venl.Type__c);
          test.stoptest();
          List<Venue_Layout__c> newlist=[select Id,Type__c,Name from Venue_Layout__c ];
          system.assertEquals(venl.Type__c, newlist[0].Type__c);
    }
    
    @IsTest static void matchInsertScenarioSquare(){
        Campaign camp = new Campaign();
        camp.name = 'old Event';
        camp.All_Day_Event__c = false;
        camp.Start_Date__c = system.now();
        camp.End_Date__c = system.now();
        camp.Type = 'A,B,C';
        insert camp;
        
        Venue__c ven=new Venue__c();
        ven.Name='vashi';
        insert ven;
        
        Venue__c venue=new Venue__c();
        venue.Name='vashi';
        insert venue;
        
        Venue_Layout__c venl=new Venue_Layout__c();
        venl.Name = 'abc';
        venl.Layout__c='custom';
        venl.Type__c='Square';
        venl.Venue__c=venue.Id;
        venl.No_of_Tables__c=10;
        venl.No_of_Seats__c=10;
        venl.Layout_Row_Count__c=10;
        insert venl;
            test.startTest();
        Venuelayoutcontroller.matchvenuelayout(venl, camp.Id, ven.Id, venl.Type__c);
        test.stoptest();
          List<Venue_Layout__c> newlist=[select Id,Type__c,Name from Venue_Layout__c ];
          system.assertEquals(venl.Type__c, newlist[0].Type__c);
    }
    
    
    @IsTest static void matchInsertScenarioConference(){
        Campaign camp = new Campaign();
        camp.name = 'old Event';
        camp.All_Day_Event__c = false;
        camp.Start_Date__c = system.now();
        camp.End_Date__c = system.now();
        camp.Type = 'A,B,C';
        insert camp;
        
        Venue__c ven=new Venue__c();
        ven.Name='vashi';
        insert ven;
        
        Venue__c venue=new Venue__c();
        venue.Name='vashi';
        insert venue;
        
        Venue_Layout__c venl=new Venue_Layout__c();
        venl.Name = 'abc';
        venl.Layout__c='custom';
        venl.Type__c='ConferenceHall';
        venl.Venue__c=venue.Id;
        venl.Conference_Type__c='open';
        venl.No_of_Seats__c=10;
        venl.Horizontal_Seats__c=10;
        insert venl;
        test.startTest();
        Venuelayoutcontroller.matchvenuelayout(venl, camp.Id, ven.Id, venl.Type__c);
         test.stoptest();
          List<Venue_Layout__c> newlist=[select Id,Type__c,Name from Venue_Layout__c ];
          system.assertEquals(venl.Type__c, newlist[0].Type__c);
    }
    
    @IsTest static void matchInsertScenarioAudience(){
        Campaign camp = new Campaign();
        camp.name = 'old Event';
        camp.All_Day_Event__c = false;
        camp.Start_Date__c = system.now();
        camp.End_Date__c = system.now();
        camp.Type = 'A,B,C';
        insert camp;
        
        Venue__c ven=new Venue__c();
        ven.Name='vashi';
        insert ven;
        
        Venue__c venue=new Venue__c();
        venue.Name='vashi';
        insert venue;
        
        Venue_Layout__c venl=new Venue_Layout__c();
        venl.Name = 'abc';
        venl.Layout__c='Plain';
        venl.Type__c='Audience';
        venl.Venue__c=venue.Id;
        venl.Sections__c=10;
        venl.No_of_Seats__c=10;
        venl.Seats_Per_Row__c=10;
        venl.Layout_Row_Count__c=10;
        insert venl;
        test.startTest();
        Venuelayoutcontroller.matchvenuelayout(venl, camp.Id, ven.Id, venl.Type__c);
           Venuelayoutcontroller.createEventVenueLayout(venl,camp.Id,ven.Id);
        test.stoptest();
          List<Venue_Layout__c> newlist=[select Id,Type__c,Name from Venue_Layout__c ];
          system.assertEquals(venl.Type__c, newlist[0].Type__c);
    }
    
    @IsTest static void matchInsertScenarioClassroom(){
        Campaign camp = new Campaign();
        camp.name = 'old Event';
        camp.All_Day_Event__c = false;
        camp.Start_Date__c = system.now();
        camp.End_Date__c = system.now();
        camp.Type = 'A,B,C';
        insert camp;
        
        Venue__c ven=new Venue__c();
        ven.Name='vashi';
        insert ven;
        
        Venue__c venue=new Venue__c();
        venue.Name='vashi';
        insert venue;
        
        Venue_Layout__c venl=new Venue_Layout__c();
        venl.Name = 'abc';
        venl.Layout__c='Plain';
        venl.Type__c='Classroom';
        venl.Venue__c=venue.Id;
        
        venl.No_of_Seats__c=10;
        
        insert venl;
         test.startTest();
        Venuelayoutcontroller.matchvenuelayout(venl, camp.Id, ven.Id, venl.Type__c);
           Venuelayoutcontroller.createEventVenueLayout(venl,camp.Id,ven.Id);
         test.stoptest();
          List<Venue_Layout__c> newlist=[select Id,Type__c,Name from Venue_Layout__c ];
          system.assertEquals(venl.Type__c, newlist[0].Type__c);
    }
    
     @IsTest static void matchInsertScenarioBooth(){
        Campaign camp = new Campaign();
        camp.name = 'old Event';
        camp.All_Day_Event__c = false;
        camp.Start_Date__c = system.now();
        camp.End_Date__c = system.now();
        camp.Type = 'A,B,C';
        insert camp;
        
        Venue__c ven=new Venue__c();
        ven.Name='vashi';
        insert ven;
        
        Venue__c venue=new Venue__c();
        venue.Name='vashi';
        insert venue;
        
        Venue_Layout__c venl=new Venue_Layout__c();
        venl.Name = 'abc';
        venl.Layout__c='Plain';
        venl.Type__c='Booth';
        venl.Venue__c=venue.Id;
        
        venl.No_of_Seats__c=10;
        
        insert venl;
         test.startTest();
        Venuelayoutcontroller.matchvenuelayout(venl, camp.Id, ven.Id, venl.Type__c);
            Venuelayoutcontroller.createEventVenueLayout(venl,camp.Id,ven.Id);
         test.stoptest();
          List<Venue_Layout__c> newlist=[select Id,Type__c,Name from Venue_Layout__c ];
          system.assertEquals(venl.Type__c, newlist[0].Type__c);
    }
    
    @IsTest static void saveDataInsertScenario(){
        Campaign camp = new Campaign();
        camp.name = 'old Event';
        camp.All_Day_Event__c = false;
        camp.Start_Date__c = date.parse('12/06/2017');
        camp.End_Date__c = date.parse('12/21/2017');
        camp.Type = 'A,B,C';
        insert camp;
        
        Campaign camp1 = new Campaign();
        camp1.name = 'olds Event';
        camp1.All_Day_Event__c = false;
        camp1.Start_Date__c = date.parse('12/26/2017');
        camp1.End_Date__c = date.parse('12/20/2017');
        camp1.Type = 'A,B,C';
        insert camp1; 
        
        Venue__c ven=new Venue__c();
        ven.Name='vashi';
        insert ven;
        
        Venue_Layout__c venl=new Venue_Layout__c();
        venl.Name = 'abc';
        venl.Layout__c='custom';
        venl.Type__c='Round';
        venl.Venue__c=ven.Id;
        insert venl;
        
        Event_venue__c event = new Event_venue__c();
        event.Name = 'abc';
        event.Campaign__c = camp1.Id;
        event.Venue__c = ven.Id;
        event.Venue_Layout__c = venl.Id;
        insert event;
          test.startTest();
        Venuelayoutcontroller.saveEventVenueLayout(venl.Id, camp.Id,ven.Id);
         test.stoptest();
          List<Event_venue__c> newlist=[select Id,Venue__c,Venue_Layout__c,Name from Event_venue__c ];
          system.assertEquals(venl.Id, newlist[0].Venue_Layout__c);
    }
    
    
    @IsTest static void saveDataCloneScenario(){
        Campaign camp = new Campaign();
        camp.name = 'old Event';
        camp.All_Day_Event__c = false;
        camp.Start_Date__c = date.parse('12/06/2017');
        camp.End_Date__c = date.parse('12/21/2017');
        camp.Type = 'A,B,C';
        insert camp;
        
        Campaign camp1 = new Campaign();
        camp1.name = 'olds Event';
        camp1.All_Day_Event__c = false;
        camp1.Start_Date__c = date.parse('12/26/2017');
        camp1.End_Date__c = date.parse('12/20/2017');
        camp1.Type = 'A,B,C';
        insert camp1; 
        
        Venue__c ven=new Venue__c();
        ven.Name='vashi';
        insert ven;
        
        Venue_Layout__c venl=new Venue_Layout__c();
        venl.Name = 'abc';
        venl.Layout__c='custom';
        venl.Type__c='Round';
        venl.Venue__c=ven.Id;
        insert venl;
        
        Event_venue__c event = new Event_venue__c();
        event.Name = 'abc';
        event.Campaign__c = camp1.Id;
        event.Venue__c = ven.Id;
        event.Venue_Layout__c = venl.Id;
        insert event;
       test.startTest();
        Venuelayoutcontroller.saveEventVenueLayout(venl.Id, camp.Id,ven.Id);
         test.stoptest();
          List<Event_venue__c> newlist=[select Id,Campaign__c,Venue_Layout__c,Name from Event_venue__c ];
          system.assertEquals(camp1.Id, newlist[0].Campaign__c);
    }
    
     @IsTest static void testvenuelayoutScenariosaquarew(){
         Campaign camp = new Campaign();
        camp.name = 'old Event';
        camp.All_Day_Event__c = false;
        camp.Start_Date__c = system.now();
        camp.End_Date__c = system.now();
        camp.Type = 'A,B,C';
        insert camp;
         
          Venue__c ven=new Venue__c();
        ven.Name='vashi';
        insert ven;
         Venue_Layout__c venl=new Venue_Layout__c();
        venl.Name = 'abc';
        venl.Layout__c='custom';
        venl.Type__c='Square';
        venl.Venue__c=ven.Id;
         venl.No_of_Tables__c=10;
        venl.No_of_Seats__c=10;
        venl.Layout_Row_Count__c=10;
          venl.Seats_Per_Row__c=10;
        insert venl;
        System.assertEquals(true, venl.Id != null, ' No user created.' + venl) ;
          test.startTest();
          Venuelayoutcontroller.createEventVenueLayout(venl,camp.Id,ven.Id);
          test.stoptest();
     }
    
    
    
}